/* SPDX-License-Identifier: 0BSD */

#include "img.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libgen.h>

#include <gd.h>

static gdImagePtr img;
static char *img_path;

int
img_loadable(const char *path)
{
	gdImagePtr load = gdImageCreateFromFile(path);
	if (!load) {
		return 0;
	}
	gdImageDestroy(load);
	return 1;
}

int
img_open(const char *path)
{
	if (img_path) {
		return -1; /* already open */
	}

	img = gdImageCreateFromFile(path);
	if (!img) {
		return -2;
	}

	img_path = strdup(path);
	if (!img_path) {
		gdImageDestroy(img);
		return -3;
	}

	return 0;
}

int
img_close(void)
{
	if (!img_path) {
		return -1; /* nothing open */
	}

	free(img_path);
	img_path = NULL;

	gdImageDestroy(img);
	img = NULL;

	return 0;
}

int
img_info(struct img_info *info)
{
	if (!img_path) {
		return -1; /* nothing open */
	}
	if (!info) {
		return -2;
	}

	info->width = gdImageSX(img);
	info->height = gdImageSY(img);
	snprintf(info->name, sizeof(info->name), "%s", basename(img_path));

	return 0;
}

int
img_color(const int x, const int y, struct img_color *color)
{
	if (!img_path) {
		return -1; /* nothing open */
	}
	if (!color) {
		return -2;
	}

	int gdlib_color = gdImageTrueColorPixel(img, x, y);
	/* TODO is this a safe conversion? */
	color->r = gdImageRed(img, gdlib_color);
	color->g = gdImageGreen(img, gdlib_color);
	color->b = gdImageBlue(img, gdlib_color);
	color->a = gdImageAlpha(img, gdlib_color);

	return 0;
}
