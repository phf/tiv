/* SPDX-License-Identifier: 0BSD */

#include "sa.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

/*
 * At some point we became convinced that we cannot "realloc" because pointers,
 * once handed out, cannot be moved. The "solution" is simply to not "hand out"
 * pointers while we're still collecting the strings. Later, when there are no
 * more strings to collect, we won't need to move anything anymore.
 */

static char *buffer;
static size_t capacity;
static size_t used;

static inline size_t
max(const size_t a, const size_t b)
{
	return a > b ? a : b;
}

int
sa_bytes(const char *mem, const size_t len)
{
	if (!mem || !len) {
		return -1;
	}

	if (len > capacity - used) {
		/* TODO investigate this heuristic in more detail? */
		/* TODO don't double `len` above a certain value? */
		size_t cap = max(capacity * 2, capacity + len * 2);
		char *buf = realloc(buffer, cap);
		if (!buf) {
			return -2;
		}
		buffer = buf;
		capacity = cap;
	}
	assert(len <= capacity - used);

	char *dst = buffer + used;
	used += len;

	memcpy(dst, mem, len);
	return 0;
}

int
sa_string(const char *str)
{
	return sa_bytes(str, strlen(str));
}

int
sa_char(const char c)
{
	return sa_bytes(&c, 1);
}

void
sa_free(void)
{
	free(buffer);
	buffer = NULL;
	capacity = 0;
	used = 0;
}

size_t
sa_index(char *strings[])
{
	size_t idx = 0;
	char *next = buffer;

	for (size_t i = 0; i < used; i++) {
		if (buffer[i] == '\0') {
			if (strings) {
				strings[idx] = next;
			}
			idx += 1;
			next = buffer + i + 1;
		}
	}

	return idx;
}
