/* SPDX-License-Identifier: 0BSD */

#include "img.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libgen.h>

#include <SDL_surface.h>
#include <SDL_image.h>

static SDL_Surface *img;

int
img_loadable(const char *path)
{
	SDL_Surface *load = IMG_Load(path);
	if (!load) {
		return 0;
	}
	SDL_FreeSurface(load);
	return 1;
}

int
img_open(const char *path)
{
	if (img) {
		return -1; /* already open */
	}

	/* Load image in whatever its native format is. */
	SDL_Surface *load = IMG_Load(path);
	if (!load) {
		return -2;
	}

	/* Convert to fixed RGBA32 format for processing. */
	img = SDL_ConvertSurfaceFormat(load, SDL_PIXELFORMAT_RGBA32, 0);
	SDL_FreeSurface(load); /* free ASAP */
	if (!img) {
		return -3;
	}

	if (SDL_LockSurface(img) < 0) {
		SDL_FreeSurface(img);
		img = NULL;
		return -4;
	}

	img->userdata = strdup(path);

	return 0;
}

int
img_close(void)
{
	if (!img) {
		return -1; /* nothing open */
	}

	free(img->userdata);

	SDL_UnlockSurface(img);
	SDL_FreeSurface(img);
	img = NULL;

	return 0;
}

int
img_info(struct img_info *info)
{
	if (!img) {
		return -1; /* nothing open */
	}
	if (!info) {
		return -2;
	}

	info->width = img->w;
	info->height = img->h;
	snprintf(info->name, sizeof(info->name), "%s", basename(img->userdata));

	return 0;
}

int
img_color(const int x, const int y, struct img_color *color)
{
	if (!img) {
		return -1; /* nothing open */
	}
	if (!color) {
		return -2;
	}

	const int bpp = img->format->BytesPerPixel;
	uint8_t *p = (uint8_t *)img->pixels + y * img->pitch + x * bpp;
	uint32_t pixel = *(uint32_t *)p;
	SDL_GetRGBA(
		pixel, img->format, &color->r, &color->g, &color->b, &color->a);

	return 0;
}
