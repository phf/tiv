/* SPDX-License-Identifier: 0BSD */

#include "img.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libgen.h>

#include <Imlib2.h>

static char *img_path;

int
img_loadable(const char *path)
{
	Imlib_Image load = imlib_load_image(path);
	return load ? 1 : 0;
}

int
img_open(const char *path)
{
	if (img_path) {
		return -1; /* already open */
	}

	Imlib_Image img = imlib_load_image(path);
	if (!img) {
		return -2;
	}
	imlib_context_set_image(img);

	img_path = strdup(path);
	if (!img_path) {
		imlib_free_image();
		return -3;
	}

	return 0;
}

int
img_close(void)
{
	if (!img_path) {
		return -1; /* nothing open */
	}

	free(img_path);
	img_path = NULL;

	imlib_free_image();

	return 0;
}

int
img_info(struct img_info *info)
{
	if (!img_path) {
		return -1; /* nothing open */
	}
	if (!info) {
		return -2;
	}

	info->width = imlib_image_get_width();
	info->height = imlib_image_get_height();
	snprintf(info->name, sizeof(info->name), "%s", basename(img_path));

	return 0;
}

int
img_color(const int x, const int y, struct img_color *color)
{
	if (!img_path) {
		return -1; /* nothing open */
	}
	if (!color) {
		return -2;
	}
	int w = imlib_image_get_width();
	int h = imlib_image_get_height();
	if (x >= w || y >= h) {
		return -3;
	}

	Imlib_Color imlib_color;
	imlib_image_query_pixel(x, y, &imlib_color);
	/* TODO is this a safe conversion? */
	color->r = imlib_color.red;
	color->g = imlib_color.green;
	color->b = imlib_color.blue;
	color->a = imlib_color.alpha;

	return 0;
}
