#pragma once
/* SPDX-License-Identifier: 0BSD */

/*
 * A very basic image API. Strictly one image at a time.
 */

#include <stdint.h>

struct img_color {
	uint8_t r, g, b, a;
};

struct img_info {
	int width, height;
	char name[256];
	/* TODO file format? etc? */
};

/*
 * Can image at given `path` be loaded?
 * Return !=0 for yes, 0 for no.
 */
int
img_loadable(const char *path);

/*
 * Open image at given `path` and make it current.
 * Return 0 for success, <0 for failure.
 */
int
img_open(const char *path);

/*
 * Close current image.
 * Return 0 for success, <0 for failure.
 */
int
img_close(void);

/*
 * Fill `info` for current image. Note that `info->name` might be truncated.
 * Return 0 for success, <0 for failure.
 */
int
img_info(struct img_info *info);

/*
 * Fill `color` for current image at pixel (`x`, `y`).
 * Return 0 for success, <0 for failure.
 */
int
img_color(int x, int y, struct img_color *color);
