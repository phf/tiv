/* SPDX-License-Identifier: 0BSD */

/*
 * tiv: Terminal Image Viewer
 *
 * Copyright (c) 2021 by Peter H. Froehlich <peter.hans.froehlich@gmail.com>
 */

#include <assert.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <poll.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "img.h"
#include "sa.h"

#define SLIDESHOW_SECS (8)

static int flag_all = 0; /* don't ignore ".hidden" files */
static int flag_verbose = 0; /* spit out extra logging */

static char **image_paths; /* array of paths to images to display */
static size_t num_images; /* size of that array */
static size_t cur_image; /* currenty image we're rendering */

static char *tixels; /* buffer of escape sequences for image rendering */
static size_t cap_tixels; /* capacity of that buffer */
static size_t off_tixels; /* offset in buffer during render */

static struct termios original_termios;

static int pipe_fd[2]; /* self-pipe for signals */

enum render_mode {
	RENDER_QUICK, /* color of top-left pixel */
	RENDER_AVERAGE, /* average color of area */
	RENDER_MAJORITY, /* majority color in area */
	NUM_RENDER_MODES,
};
static enum render_mode cur_mode = RENDER_AVERAGE;

static void
slog(const char *format, ...)
{
	if (!flag_verbose) {
		return;
	}

	va_list args;
	va_start(args, format);

	vfprintf(stderr, format, args);

	va_end(args);
}

static void
warn(const char *format, ...)
{
	va_list args;
	va_start(args, format);

	vfprintf(stderr, format, args);

	va_end(args);
}

static void
msg(const char *format, ...)
{
	va_list args;
	va_start(args, format);

	vfprintf(stdout, format, args);

	va_end(args);
}

_Noreturn void
die(const char *format, ...)
{
	va_list args;
	va_start(args, format);

	vfprintf(stderr, format, args);

	va_end(args);
	exit(EXIT_FAILURE);
}

static void
add_file(const char *path, const char *name)
{
	assert(path && name);

	if (name[0] == '.' && !flag_all) {
		slog("ignore hidden '%s' file\n", name);
		return;
	}

	static char buf[4096];
	int res = snprintf(buf, sizeof(buf), "%s/%s", path, name);
	if (res < 0 || res >= (signed)sizeof(buf)) {
		warn("ignore truncated '%s' path\n", buf);
		return;
	}

	if (!img_loadable(buf)) {
		slog("ignore non-image '%s' path\n", buf);
		return;
	}

	sa_string(buf);
	sa_char('\0');
}

static void
add_dir(char *path)
{
	/* get rid of trailing slashes, not required for correctness */
	size_t len = strlen(path);
	while (len > 1 && path[len - 1] == '/') {
		path[len - 1] = '\0'; /* modifies path! */
		len -= 1;
	}

	DIR *dir = opendir(path);
	if (!dir) {
		warn("failed to opendir '%s'\n", path);
		return;
	}

	struct dirent *dp;
	while ((dp = readdir(dir))) {
		if (dp->d_type != DT_REG) {
			slog("skipped non-file '%s'\n", dp->d_name);
			continue;
		}
		add_file(path, dp->d_name);
	}

	closedir(dir);
}

static void
add_path(char *path)
{
	struct stat s;
	if (stat(path, &s)) {
		warn("failed to stat '%s'\n", path);
		return;
	}
	if (S_ISDIR(s.st_mode)) {
		add_dir(path);
	} else if (S_ISREG(s.st_mode)) {
		add_file(dirname(path), basename(path));
	} else {
		slog("neither file nor directory '%s'", path);
	}
}

static void
restore_cooked_mode(void)
{
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &original_termios);
	/* fix terminal state as best we can */
	printf("\x1b[0m"); /* reset colors */
	printf("\x1b[?25h"); /* cursor on */
	fflush(stdout);
}

static void
enter_raw_mode(void)
{
	tcgetattr(STDIN_FILENO, &original_termios);
	if (atexit(restore_cooked_mode) != 0) {
		die("cannot register exit handler restore_cooked_mode");
	}
	struct termios raw = original_termios;
	raw.c_lflag &= ~(ECHO | ICANON);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);

	printf("\x1b[?25l"); /* cursor off */
	fflush(stdout);
}

static int
get_dimensions(int *width, int *height)
{
	if (!width || !height) {
		return -1;
	}

	/* first try an ioctl to get the size */
	struct winsize ws;
	if (ioctl(STDIN_FILENO, TIOCGWINSZ, &ws) != -1) {
		*width = ws.ws_col;
		*height = ws.ws_row;
		/* ws_ypixel and ws_xpixel are useless */
		return 0;
	}

	/* then fall back to escape sequences */
	printf("\x1b[999;999H"); /* move cursor *way* over there */
	printf("\x1b[6n"); /* where's the cursor? */
	fflush(stdout);
	if (fscanf(stdin, "\x1b[%d;%dR", height, width) == 2) {
		return 0;
	}

	return -2;
}

static int
alloc_tixels(const int width, const int height)
{
	assert(width > 0 && height > 0);

	/* bytes required for one white "tixel" */
	static const size_t maxpix = sizeof("\x1b[48;2;255;255;255m ") - 1;

	/* reallocate larger buffer if we need to */
	const size_t needed = width * height;
	if (needed * maxpix > cap_tixels) {
		char *buf = reallocarray(tixels, needed, maxpix);
		if (!buf) {
			return -1;
		}
		tixels = buf;
		cap_tixels = needed * maxpix;
	}
	return 0;
}

static void
reset_tixels()
{
	off_tixels = 0;
	tixels[0] = '\0';
}

static int
append_tixel(struct img_color color)
{
	int res = snprintf(tixels + off_tixels, cap_tixels - off_tixels,
		"\x1b[48;2;%u;%u;%um ", color.r, color.g, color.b);
	if (res < 14 || res > 20) {
		/* something must be wrong since we should get at least 14 and
		 * at most 20 chars */
		return -1;
	}

	off_tixels += res;
	if (off_tixels > cap_tixels) {
		/* something's wrong since we exceeded the buffer */
		return -2;
	}

	return 0;
}

static void
signal_handler(int sig)
{
	char buf = sig;
	write(pipe_fd[1], &buf, 1);
}

static int
create_signal_pipe(int fds[2])
{
	/* we are about to squeeze signals through a pipe */
	static_assert(SIGWINCH <= CHAR_MAX, "doesn't fit into char");
	static_assert(SIGINT <= CHAR_MAX, "doesn't fit into char");

	if (pipe2(fds, O_NONBLOCK) == -1) {
		return -1;
	}

	if (signal(SIGWINCH, signal_handler) == SIG_ERR) {
		return -2;
	}
	if (signal(SIGINT, signal_handler) == SIG_ERR) {
		return -3;
	}

	return 0;
}

static struct img_color
average_color(int x, int y, int w, int h)
{
	size_t r = 0, g = 0, b = 0, a = 0, n = 0;
	for (int i = 0; i < w; i++) {
		for (int j = 0; j < h; j++) {
			struct img_color c;
			if (img_color(x + i, y + j, &c) < 0) {
				continue;
			}
			r += c.r;
			g += c.g;
			b += c.b;
			a += c.a;
			n += 1;
		}
	}
	return n > 0 ? (struct img_color){r / n, g / n, b / n, a / n}
		     : (struct img_color){0, 0, 0, 0};
}

static int
equal_color(struct img_color p, struct img_color q)
{
	return p.r == q.r && p.g == q.g && p.b == q.b && p.a == q.a;
}

static struct img_color
majority_color(int x, int y, int w, int h)
{
	/* TODO init to 0 bias to 0? should we init to top-left pixel? */
	struct img_color candidate = {0};
	size_t lead = 0;

	for (int i = 0; i < w; i++) {
		for (int j = 0; j < h; j++) {
			struct img_color c;
			if (img_color(x + i, y + j, &c) < 0) {
				continue;
			}

			if (lead == 0) {
				candidate = c;
				lead = 1;
			} else if (equal_color(candidate, c)) {
				lead++;
			} else {
				lead--;
			}
		}
	}

	return candidate;
}

void
render(const char *path, const enum render_mode mode)
{
	int W, H;
	if (get_dimensions(&W, &H) < 0) {
		return;
	}

	if (alloc_tixels(W, H) < 0) {
		return;
	}

	if (img_open(path) < 0) {
		return;
	}

	struct img_info info;
	if (img_info(&info) < 0) {
		return;
	}

	/* TODO aspect ratio */
	/* TODO alpha if there is any */

	printf("\x1b[;H"); /* cursor home */
	fflush(stdout);

	/* how many pixels for one character? */
	const int dx = info.width / W;
	const int dy = info.height / H;
	/* how many pixels left over to distribute? */
	const int rx = info.width % W;
	const int ry = info.height % H;

	reset_tixels(); /* clear tixel buffer */

	int Y = 0;
	for (int y = 0; y < H; y++) {
		int ey = dy;
		if (y < ry / 2 || y >= H - ry / 2) {
			ey += 1;
		}
		int X = 0;
		for (int x = 0; x < W; x++) {
			int ex = dx;
			if (x < rx / 2 || x >= W - rx / 2) {
				ex += 1;
			}

			struct img_color color;

			switch (mode) {
			case RENDER_QUICK:
				img_color(X, Y, &color);
				break;
			case RENDER_AVERAGE:
				color = average_color(X, Y, ex, ey);
				break;
			case RENDER_MAJORITY:
				color = majority_color(X, Y, ex, ey);
				break;
			default:
				die("invalid render mode");
				break;
			}

			if (append_tixel(color)) {
				die("failed to append tixel");
			}
			X += ex;
		}
		Y += ey;
	}
	write(STDOUT_FILENO, tixels, strlen(tixels));
	/* TODO proper info */
	printf("\x1b[;H%s", info.name);
	fflush(stdout);

	img_close();
}

static void
parse_arguments(int argc, char *argv[])
{
	/*
	 * TODO note that for --all and --verbose the flag only takes effect at
	 * the point it appears in the argument vector, is that a problem?
	 */
	for (int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--version")) {
			msg("terminal image viewer v%s (git %s, using %s)\n",
				VERSION, GITTAG, IMGMOD);
			exit(EXIT_SUCCESS);
		}
		if (!strcmp(argv[i], "--help")) {
			msg("usage: tiv [--version] [--help] [--all] "
			    "[--verbose] [path ...]\n");
			exit(EXIT_SUCCESS);
		}
		if (!strcmp(argv[i], "--all")) {
			flag_all = 1;
			continue;
		}
		if (!strcmp(argv[i], "--verbose")) {
			flag_verbose = 1;
			continue;
		}
		if (argv[i][0] == '-') {
			warn("ignore unknown flag '%s'\n", argv[i]);
			continue;
		}
		add_path(argv[i]);
	}
}

static void
setup_index(void)
{
	size_t count = sa_count();
	if (!count) {
		/*
		 * all the earlier parameter stuff resulted in nothing; give it
		 * one more shot with the current directory!
		 */
		add_path(".");

		count = sa_count();
		if (!count) {
			/* nothing to display; see `exit` in parse_arguments */
			exit(EXIT_SUCCESS);
		}
	}

	image_paths = calloc(count, sizeof(*image_paths));
	if (!image_paths) {
		die("out of memory\n");
	}
	size_t check = sa_index(image_paths);
	assert(count == check);

	num_images = count;
}

/*
 * TODO in both handle_signal and handle_input it's debatable why we use a
 * buffer of a certain size and why we process (potentially) multiple bytes;
 * as of right now, the only answer I have is "why not?" but that's not great
 */

static void
handle_signal(const int fd)
{
	ssize_t res;
	char buf[16];
	if ((res = read(fd, buf, sizeof(buf))) > 0) {
		for (int i = 0; i < res; i++) {
			if (buf[i] == SIGINT) {
				exit(EXIT_SUCCESS);
			}
			if (buf[i] == SIGWINCH) {
				/* nothing to do, we'll redraw */
			}
		}
	}
}

static void
handle_input(const int fd)
{
	ssize_t res;
	char buf[16];
	if ((res = read(fd, buf, sizeof(buf))) > 0) {
		for (int i = 0; i < res; i++) {
			char c = buf[i];
			if (c == 'q') {
				exit(EXIT_SUCCESS);
			}
			/*
			 * lots of keys generate sequences that start with ESC
			 * for example the cursor keys; hence we added a hack
			 * here to only exit if it's ESC by itself; in the end
			 * we'll probably have to REALLY parse what comes down
			 * stdin but so far we can get away with less; although
			 * strictly speaking, someone typing really fast might
			 * mess this up for us? TODO
			 */
			if (c == 27 && res == 1) {
				exit(EXIT_SUCCESS);
			}
			if (c == 'r') {
				cur_mode = (cur_mode + 1) % NUM_RENDER_MODES;
			}
			if (c == ' ' || c == 'n' || c == 'j' || c == 'l') {
				cur_image = (cur_image + 1) % num_images;
			}
			if (c == 127 || c == 'b' || c == 'h' || c == 'k') {
				cur_image = (cur_image + num_images - 1) %
					    num_images;
			}
			/* TODO various info modes, aspect ration modes */
		}
	}
}

static void
display_images(void)
{
	enter_raw_mode();
	create_signal_pipe(pipe_fd);

	struct pollfd fds[2] = {0};
	fds[0].fd = STDIN_FILENO;
	fds[0].events = POLLIN;
	fds[1].fd = pipe_fd[0];
	fds[1].events = POLLIN;

	for (;;) {
		render(image_paths[cur_image], cur_mode);

		int res = poll(fds, 2, SLIDESHOW_SECS * 1000);
		if (res == -1) {
			if (errno == EINTR) {
				/*
				 * self-pipe be tricky: signals aren't blocked,
				 * so when one gets delivered, poll is (most
				 * likely) interrupted; but our handler ran and
				 * put something into our pipe; so the next poll
				 * will return with the pipe ready
				 */
				continue;
			}
			die("poll exploded: %d %s\n", errno, strerror(errno));
		}
		if (res == 0) {
			/* timed out so let's slideshow this thing */
			cur_image = (cur_image + 1) % num_images;
			continue;
		}

		if ((fds[0].revents & (POLLERR | POLLNVAL))) {
			die("stdin clogged: %d %s\n", errno, strerror(errno));
		}
		if ((fds[1].revents & (POLLERR | POLLNVAL))) {
			die("pipe burst: %d %s\n", errno, strerror(errno));
		}

		if (fds[0].revents & POLLIN) {
			handle_input(fds[0].fd);
			continue;
		}
		if (fds[1].revents & POLLIN) {
			handle_signal(fds[1].fd);
			continue;
		}

		die("should not have happened\n");
	}

	die("whoa!\n");
}

static void
cleanup(void)
{
	free(image_paths);
	sa_free();
}

int
main(int argc, char *argv[])
{
	parse_arguments(argc, argv);
	setup_index();
	display_images();
	cleanup();
	exit(EXIT_SUCCESS);
}
