#pragma once
/* SPDX-License-Identifier: 0BSD */

/*
 * String allocator. Keep strings around, dynamically allocated in a single
 * ever-growing buffer instead of all over the place. Note that NUL bytes have
 * to be placed explicitly to separate strings (see below).
 */

#include <stdlib.h>

/*
 * Append first `len` bytes from `mem` to tbe buffer; NUL-bytes are appended as
 * well, no questions asked.
 * Return 0 on success, <0 on failure.
 */
int
sa_bytes(const char *mem, const size_t len);

/*
 * Append `strlen(str)` characters from `str` to the buffer.
 * Return 0 on success, <0 on failure.
 */
int
sa_string(const char *str);

/*
 * Append character `c` to the buffer. Use this to place explicit NUL bytes
 * between finished strings (if you've been building them up from parts).
 * Return 0 on success, <0 on failure.
 */
int
sa_char(const char c);

/*
 * Free all dynamically allocated strings, invalidating any pointers returned
 * by `sa_index`.
 */
void
sa_free(void);

/*
 * Fill `strings` with pointers to all the NUL-terminated strings currently in
 * the buffer.
 * Return number >=0 of strings indexed.
 *
 * Of course `strings` must be large enough, but one can pass `strings == NULL`
 * and no index will be created but the number of strings that would have been
 * indexed is still returned. See `sa_count` below.
 */
size_t
sa_index(char *strings[]);

static inline size_t
sa_count(void)
{
	return sa_index(NULL);
}
