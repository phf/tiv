# tiv: Terminal Image Viewer

View (low-resolution approximations of) images in your terminal window.
For decent results your terminal should show its
[true colors](https://github.com/termstandard/colors); both
[st](https://st.suckless.org/) and the venerable
[xterm](https://invisible-island.net/xterm/) tend to work.

## Motivation

Have you ever been ssh'd into some server and needed to preview an image for
some web thingy?
Now you can, right from your terminal window.
A bit blocky maybe, but hey, at least you didn't have to download it, view it
locally, and then remember to delete it again.

![if this is the actual image](tn_adnd.jpg)
![then this is what tiv shows you](tn_tiv.jpg)

## Usage

You can start `tiv` without any arguments in which case it will try to display
images from the current directory; you can also start `tiv` with a path to a
directory in which case it will try to display images from that directory; or
you can start `tiv` with as many paths to files and directories as you would
like:

```
tiv
tiv /home/john/Downloads
tiv image.png /usr/local/share/*/*.jpg
tiv /home/john/Downloads ./ /usr/local/share/*/*.jpg
```

Once `tiv` has displayed the first image, it waits for input or certain other
events:

- press `ESC` or 'q' to quit
- press `SPACE` or 'n' or 'j' or 'l' to cycle forward to the next image
- press `DEL` or 'b' or 'h' or 'k' to cycle backward to the previous image
  - note that `DEL` is often what's generated by the `BACKSPACE` key 🤷
- press 'r' to cycle through various rendering modes

Changing the size of the terminal window redraws the image; after 8 seconds
of inactivity `tiv` cycles forward to the next image automatically.

## Dependencies

- either [imlib2](https://git.enlightenment.org/old/legacy-imlib2) by default
- or [sdl2](https://www.libsdl.org/) if preferred
- or [libgd](https://libgd.github.io/) if preferred

All of these seem horrible in slightly different ways, but nobody wants to see
yet another one, do they?

## Build

It's not `autoconf` but a short (by comparison) shell script.

```
./configure
make
make install
```

Say `./configure --with-sdl2` or `./configure --with-libgd` if you prefer
one of those libraries instead; add `--debug` for a debug build.
Tested on Linux and OpenBSD.
(Note that GNU make will use the `GNUmakefile` so `just make` should work on
Linux and on OpenBSD.)

## Patches

I am happy to receive patches that remain true to the spirit of this hack.
If you want to turn `tiv` into `geeqie` then maybe you should just fork.

## Kudos

- [cube.c](https://c9x.me/cbits/cube.c) inspiration
- [kilo.c](https://github.com/antirez/kilo) bits of code and inspiration
- [timg](https://timg.sh/) is a *much* more advanced terminal image viewer

## License

Software should be as free as possible and this program is no exception.
Since it's under a 0-clause BSD license, which essentially makes it public
domain, you're welcome to do with it as you please.
If the program, or even just parts of the source code, was helpful to you
I'd appreciate an email, but it's certainly not required.
